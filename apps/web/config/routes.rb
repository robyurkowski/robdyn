get '/contact', to: 'contacts#index'
post '/contact', to: 'contacts#create'

get '/health', to: ->(env) { [200, {}, []] }
