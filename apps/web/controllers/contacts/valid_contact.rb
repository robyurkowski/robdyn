require 'to_regexp'

class ValidContact

  attr_accessor :gotcha,
                :reply_to,
                :message,
                :spamterms


  def initialize(gotcha:, reply_to:, message:, spamterms: nil)
    self.gotcha     = gotcha
    self.reply_to   = reply_to
    self.message    = message
    self.spamterms  = spamterms || ENV['SPAMTERMS'].split(",").map(&:to_regexp) rescue []
  end


  def valid?
    gotcha_nil? && reply_to_not_nil? && message_free_from_spammy_terms?
  end


  private def gotcha_nil?
    gotcha.nil? || gotcha == ""
  end


  private def reply_to_not_nil?
    !(reply_to.nil? || reply_to == "")
  end


  private def message_free_from_spammy_terms?
    spamterms.none? { |spamterm| spamterm.match message }
  end
end
