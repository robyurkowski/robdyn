require 'rest_client'
require_relative './valid_contact'

module Web::Controllers::Contacts
  class Create
    include Web::Action

    def call(params)
      contact = ValidContact.new(
        gotcha:     params[:_gotcha],
        reply_to:   params[:_replyto],
        message:    params[:message]
      )

      return redirect_to redirect_failure_url unless contact.valid?

      ::RestClient.post "https://api:#{ENV['MAILGUN_API_KEY']}@api.mailgun.net/v2/#{ENV['MAILGUN_DOMAIN']}/messages",
        :from     =>  ENV['CONTACT_SENDER_EMAIL'],
        :to       =>  ENV['CONTACT_FORWARDING_EMAIL'],
        :subject  =>  "New contact from your website",
        :html     =>  email_body,
        :"h:Reply-To" => params[:_replyto]

      return redirect_to redirect_success_url
    end


    def redirect_success_url
      case params[:_site].downcase
      when 'wwt'
        ENV['WWT_SUCCESS_REDIRECT_URL']
      else
        ENV['CONTACT_SUCCESS_REDIRECT_URL']
      end
    end


    def redirect_failure_url
      case params[:_site].downcase
      when 'wwt'
        ENV['WWT_FAILURE_REDIRECT_URL']
      else
        ENV['CONTACT_FAILURE_REDIRECT_URL']
      end
    end


    def email_body
      filename = File.expand_path(File.join(File.dirname(__FILE__), 'create_email.html'))
      body = File.read(filename)
      body.gsub!("{name}", params[:name])
      body.gsub!("{replyto}", params[:_replyto])
      body.gsub!("{gotcha}", params[:_gotcha].inspect)
      body.gsub!("{message}", params[:message])
      body.gsub!("{timestamp}", Time.now.to_s)
      body.gsub!("{site}", params[:site].downcase || "rob-blog")
      body
    end
  end
end
